﻿using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagInit
{
    public class BagInit : AOPluginEntry
    {
        bool InitBags = true;

        List<BCKCTRL> backpacksctrl = new List<BCKCTRL>();

        DateTime InitDutyTime=DateTime.Now;

        DateTime CommandDutyTime = DateTime.Now;



        public override void Run(string pluginDir)
        {
            Chat.WriteLine("BagInit loaded");
            Game.TeleportEnded += TeleportEnded;
            Game.OnUpdate+= OnUpdate;
            Init();
        }

        private void TeleportEnded(object sender, EventArgs e)
        {
            Init();
        }

        private void OnUpdate(object sender, float e)
        {
            if (Game.IsZoning) return;

            if ((DateTime.Now - InitDutyTime).TotalSeconds < 5)
            {
                //Chat.WriteLine("Duty time");
                return;
            }

            if (!InitBags) return;

            if ((DateTime.Now - CommandDutyTime).TotalSeconds < 5) return;
                    
            foreach (BCKCTRL bckct in backpacksctrl)
            {
                Item.Use(bckct.backpack.Slot);
            }

            CommandDutyTime = DateTime.Now;
            step++;
            if (step > 1)
            {
                InitBags = false;
            }


        }
        int step = 0;

        void Init()
        {
            step = 0;
            InitBags = true;
            backpacksctrl = new List<BCKCTRL>();
            foreach (Backpack bck in Inventory.Backpacks
                )
            {
                BCKCTRL bckct = new BCKCTRL();
                bckct.backpack = bck;
                backpacksctrl.Add(bckct);
            }
            InitDutyTime = DateTime.Now;
        }
    }


    public class BCKCTRL
    {
        public Backpack backpack;

    }
    
}
